#include <iostream>
#include <fstream>
#include<string>
#include<vector>
using namespace std;


// Tworzymy strukturę film, która będzie przechowywała wartość numeru oraz oceny, a także nazwę
struct Film
{
    int numer;
    string nazwa;
    float ocena;
};


// funkcja wyluskujaca z linijki ocene, numer oraz nazwe filmu
bool wczytajFilm(Film* film, string line)
{
    // musi miec conajmniej 7 znakow, bo w najmniejszym przypadku bedzie, np. 1,"a",3. W innym przypadku funkcja zwraca false
    if (line.size() < 7)
    {
        return false;
    }
    // jesli nie znajdzie zadnego przecinka to zwraca false
    std::size_t found = line.find(",");
    if (found == std::string::npos)
    {
        cout << "falsz";
        return false;
    }
    // ze stringa konwertujemy numer filmu na wartosc int. Tym stringiem bedzie wartosc od zerowego indeksu linijki do pierwszego przecinka
    film->numer = stoi(line.substr(0, found));

    // linia teraz to tylko string po pierwszym przecinku
    line = line.substr(found + 1, line.size() - found);

    // szuka ostatniego wystepujacego przecinka, jesli nie znajdzie zwraca fałsz
    found = line.find_last_of(",");
    if (found == std::string::npos)
    {
        return false;
    }

    // Jesli nie ma kropki po ocenie, to patrzac na zapis w pliku nie ma tam oceny.
    if (line.find(".", found + 2) == std::string::npos)
    {
        return false;
    }
    // po ostatnim przecinku szuka stringa az do konca linijki i to bedzie nasza ocena filmu
    string ocena = line.substr(found + 1, line.size());
    //if (ocena.size() == 0)
    //{
    //    return false;
    //}

    // nazwa to string od momentu po pierwszym przecinku do ostatniego przecinka
    film->nazwa = line.substr(0, found);

    // konwerturemy stringa na floata
    film->ocena = atof(ocena.c_str());

    // cout << "numer:" << film->numer << endl;
    // cout << "line: [" << film->nazwa << "]" << endl;
     //scout << "ocena:" << film->ocena << endl;

    return true;
}


// funkcja wczytujaca zadana ilosc filmow
Film** wczytajFilmy(int& ilosc, string nazaPliku)
{
    // tablica wskaznicow na zmienne typu Film
    Film** filmy = new Film * [ilosc];
    ifstream plik(nazaPliku);
    // sprawdzamy poprawnosc pliku
    if (!plik.good())
    {
        cout << "nie poprawny plik";
    }
    string line;
    //odczytanie linii naglowka
    getline(plik, line);
    vector<Film*> v;
    int rzeczywistaIlosc = 0;
    while (((rzeczywistaIlosc < ilosc) || (ilosc < 0)) && plik.peek() != EOF)
    {
        getline(plik, line);
        filmy[rzeczywistaIlosc] = new Film;
        if (!wczytajFilm(filmy[rzeczywistaIlosc], line))
        {
            //   cout << line;
            delete filmy[rzeczywistaIlosc];
        }
        else
        {
            rzeczywistaIlosc++;
        }
    }
    ilosc = rzeczywistaIlosc;
    cout << "Rzeczywista ilosc wczytanych filmow: " << rzeczywistaIlosc << endl;
    plik.close();
    return filmy;
}


// usuwanie filmow z tablicy
void skasujFilmy(int ilosc, Film** filmy)
{
    for (int i = 0; i < ilosc; i++)
    {
        delete filmy[i];
    }
    delete[] filmy;
}

bool poprawniePosortowane(Film** tab, int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        if (tab[i]->ocena > tab[i + 1]->ocena)
        {
            return false;
        }
    }
    return true;
}

// sortowanie przez kopcowanie
void kopcujMax(Film** arr, int n, int i)
{
    int largest = i; // Największy jako korzen
    int l = 2 * i + 1; // lewy = 2*i + 1
    int r = 2 * i + 2; // prawy = 2*i + 2

    // Jesli lewy jest wiekszy od korzenia
    if (l < n && arr[l]->ocena > arr[largest]->ocena)
        largest = l;

    // Jesli prawy jest wiekszy do dotychczasowego najwiekszego
    if (r < n && arr[r]->ocena > arr[largest]->ocena)
        largest = r;

    // Jesli najwiekszy nie jest korzeniem, czyli jesli largest sie zmienil podczas wykonywania
    if (largest != i) {
        swap(arr[i], arr[largest]);

        // Rekurencyjnie kopcujemy aktualna galaz
        kopcujMax(arr, n, largest);
    }
}

void sortowanie_przez_kopcowanie(Film** arr, int n)
{
    // budowanie kopca (przeksztalcenie tablicy)
    for (int i = n / 2 - 1; i >= 0; i--)
        kopcujMax(arr, n, i);

    // po kolei wyciagamy element z kopca
    for (int i = n - 1; i > 0; i--) {
        // przenies aktualny korzen na koniec
        swap(arr[0], arr[i]);

        // wywolanie kopcujMax na zredukowanym kopcu
        kopcujMax(arr, i, 0);
    }
}



//scalenie posortowanych podtablic
void scal(Film** tab, Film** pom, int lewy, int srodek, int prawy)
{
    int i, j;

    //zapisujemy lewą częsć podtablicy w tablicy pomocniczej
    for (i = srodek + 1; i > lewy; i--)
        pom[i - 1] = tab[i - 1];

    //zapisujemy prawą częsć podtablicy w tablicy pomocniczej
    for (j = srodek; j < prawy; j++)
        pom[prawy + srodek - j] = tab[j + 1];

    //scalenie dwóch podtablic pomocniczych i zapisanie ich 
    //we własciwej tablicy
    for (int k = lewy; k <= prawy; k++)
        if (pom[j]->ocena < pom[i]->ocena)
            tab[k] = pom[j--];
        else
            tab[k] = pom[i++];
}

void sortowanie_przez_scalanie(Film** tab, Film** pom, int lewy, int prawy)
{
    //gdy mamy jeden element, to jest on już posortowany
    if (prawy <= lewy) return;

    //znajdujemy srodek podtablicy
    int srodek = (prawy + lewy) / 2;

    //dzielimy tablice na częsć lewą i prawa
    sortowanie_przez_scalanie(tab, pom, lewy, srodek);
    sortowanie_przez_scalanie(tab, pom, srodek + 1, prawy);

    //scalamy dwie już posortowane tablice
    scal(tab, pom, lewy, srodek, prawy);
}

void shell(Film** tab, int N)
{
    // Wyznaczamy wartość początkowego przesunięcia
    int h;
    for (h = 1; h < N; h = 3 * h + 1);
    h /= 9;
    if (!h) h++; // istotne dla małych N, dla większych można pominąć!

  // Sortujemy

    while (h)
    {
        for (int j = N - h - 1; j >= 0; j--)
        {
            Film* x = tab[j];
            int i = j + h;
            while ((i < N) && (x->ocena > tab[i]->ocena))
            {
                tab[i - h] = tab[i];
                i += h;
            }
            tab[i - h] = x;
        }
        h /= 3;
    }
}



void quick_sort(Film** tab, int lewy, int prawy)
{
    if (prawy <= lewy) return;

    int i = lewy - 1, j = prawy + 1,
        pivot = tab[(lewy + prawy) / 2]->ocena; //wybieramy punkt odniesienia

    while (1)
    {
        //szukam elementu wiekszego lub rownego piwot stojacego
        //po prawej stronie wartosci pivot
        while (pivot > tab[++i]->ocena);

        //szukam elementu mniejszego lub rownego pivot stojacego
        //po lewej stronie wartosci pivot
        while (pivot < tab[--j]->ocena);

        //jesli liczniki sie nie minely to zamień elementy ze soba
        //stojace po niewlasciwej stronie elementu pivot
        if (i <= j)
            //funkcja swap zamienia wartosciami tab[i] z tab[j]
            swap(tab[i], tab[j]);
        else
            break;
    }

    if (j > lewy)
        quick_sort(tab, lewy, j);
    if (i < prawy)
        quick_sort(tab, i, prawy);
}


// uruchomienie wszystkich sortowan dla zadanej ilosci danych
void test10tys(int ileTys)
{
    int tys = 1000;
    if (ileTys < 0)
    {
        ileTys = 1011;
    }
    int ilosc = ileTys * tys;
    Film** filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    cout << ilosc << endl;
    sortowanie_przez_kopcowanie(filmy, ilosc);
    for (int i = 0; i < ilosc; i++)
    {
        cout << filmy[i]->ocena << " ";
    }
    if (!poprawniePosortowane(filmy, ilosc))
    {
        cout << "Sortowanie nie poprawne" << endl;
    }
    skasujFilmy(ilosc, filmy);

    cout << "sortowanie przez scalanie" << endl;
    filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    Film** pom = new Film * [ilosc];
    sortowanie_przez_scalanie(filmy, pom, 0, ilosc - 1);
    for (int i = 0; i < ilosc; i++)
    {
        cout << filmy[i]->ocena << " ";
    }
    if (!poprawniePosortowane(filmy, ilosc))
    {
        cout << "Sortowanie nie poprawne" << endl;
    }
    skasujFilmy(ilosc, filmy);

    delete pom;
    cout << "shell" << endl;
    filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    shell(filmy, ilosc);
    for (int i = 0; i < ilosc; i++)
    {
        cout << filmy[i]->ocena << " ";
    }
    if (!poprawniePosortowane(filmy, ilosc))
    {
        cout << "Sortowanie nie poprawne" << endl;
    }
    skasujFilmy(ilosc, filmy);

    cout << "quick sort" << endl;
    filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    quick_sort(filmy, 0, ilosc - 1);
    for (int i = 0; i < ilosc; i++)
    {
        cout << filmy[i]->ocena << " ";
    }
    if (!poprawniePosortowane(filmy, ilosc))
    {
        cout << "Sortowanie nie poprawne" << endl;
    }
    skasujFilmy(ilosc, filmy);
}


void sprawdzenie_czasu_kopcowanie(int ileTys)
{
    int tys = 1000;
    if (ileTys < 0)
    {
        ileTys = 1011;
    }
    int ilosc = ileTys * tys;

    Film** filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    cout << ilosc << endl;
    cout << endl;
    cout << "sortowanie przez kopcowanie" << endl;


    clock_t start, stop;
    start = clock();
    sortowanie_przez_kopcowanie(filmy, ilosc);
    stop = clock();
    skasujFilmy(ilosc, filmy);

    double czas = (double)(stop - start) / CLOCKS_PER_SEC;
    cout << "czas dzialania programu: " << czas << endl;
}


void sprawdzenie_czasu_scalanie(int ileTys)
{
    int tys = 1000;
    if (ileTys < 0)
    {
        ileTys = 1011;
    }
    int ilosc = ileTys * tys;

    Film** filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    cout << "sortowanie przez scalanie" << endl;
    cout << ilosc << endl;

    clock_t start, stop;
    start = clock();
    Film** pom = new Film * [ilosc];
    sortowanie_przez_scalanie(filmy, pom, 0, ilosc - 1);
    stop = clock();
    skasujFilmy(ilosc, filmy);
    delete pom;

    double czas = (double)(stop - start) / CLOCKS_PER_SEC;
    cout << "czas dzialania programu: " << czas << endl;
}


void sprawdzenie_czasu_shell(int ileTys)
{
    int tys = 1000;
    if (ileTys < 0)
    {
        ileTys = 1011;
    }
    int ilosc = ileTys * tys;

    Film** filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    cout << "sortowanie shell" << endl;
    cout << ilosc << endl;

    clock_t start, stop;
    start = clock();
    shell(filmy, ilosc);
    stop = clock();
    skasujFilmy(ilosc, filmy);

    double czas = (double)(stop - start) / CLOCKS_PER_SEC;
    cout << "czas dzialania programu: " << czas << endl;
}

void sprawdzenie_czasu_quicksort(int ileTys)
{
    int tys = 1000;
    if (ileTys < 0)
    {
        ileTys = 1011;
    }
    int ilosc = ileTys * tys;

    Film** filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    cout << "sortowanie quick sort" << endl;
    cout << ilosc << endl;

    clock_t start, stop;
    start = clock();
    quick_sort(filmy, 0, ilosc - 1);
    stop = clock();
    skasujFilmy(ilosc, filmy);
    double czas = (double)(stop - start) / CLOCKS_PER_SEC;
    cout << "czas dzialania programu: " << czas << endl;
}


void srednia_i_mediana(int& ilosc, float& srednia, float& mediana)
{
    float suma = 0;
    Film** filmy = wczytajFilmy(ilosc, "projekt2_dane.csv");
    for (int i = 0; i < ilosc; i++)
    {
        suma += filmy[i]->ocena;
    }
    srednia = (float)(suma / ilosc);
    if (ilosc % 2 == 0)
    {
        mediana = (filmy[ilosc / 2]->ocena + filmy[ilosc / 2 - 1]->ocena) / 2;
    }
    else
    {
        mediana = filmy[(ilosc - 1) / 2]->ocena;
    }
}
int main()
{
    float srednia;
    float mediana;
    int ile_danych = 5;
    test10tys(1);
    sprawdzenie_czasu_kopcowanie(1010.294);
    sprawdzenie_czasu_scalanie(700);
    sprawdzenie_czasu_shell(700);
    sprawdzenie_czasu_quicksort(700);
    srednia_i_mediana(ile_danych, srednia, mediana);
    cout << endl << "SREDNIA = " << srednia << ", MEDIANA = " << mediana << endl;
    //test10tys(10);
    //test10tys(100);
    //test10tys(500);
    //test10tys(1000);
    //test10tys(-1);

}
