﻿#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include "Plansza.h"
#include "RuchIPozycja.h"
#include "MozliweRuchy.h"

using namespace std;

struct pozycja poz(int x, int y)
{
	struct pozycja pozycja;
	pozycja.x = x;
	pozycja.y = y;
	return pozycja;
}

int MinMax(Plansza* w, char uzytkownik, int poziom , int alfa, int beta, struct ruch* wybranyRuch)
{
	if (poziom == 0)
	{
		int oc = w->ocena(uzytkownik);
		return oc;
	}
	MozliweRuchy mozliweRuchy(w);

	struct elementlisty* ruchy = mozliweRuchy.dajPoczatek();

	int stop = 0;
	while (ruchy != NULL && !stop)
	{
		struct ruch ruch = ruchy->ruch;
		ruchy = ruchy->nast;

		///zapamietujemy poczatkowy stan przed wykonaniem ruchu
		char poczatek = w->pola[ruch.poczatkowa.y][ruch.poczatkowa.x];
		char koniec = w->pola[ruch.koncowa.y][ruch.koncowa.x];
		char zbity;
		if (ruch.zbity.x != -1 && ruch.zbity.y != -1)  //jesli w tym ruchu jest bicie
		{
			zbity = w->pola[ruch.zbity.y][ruch.zbity.x];
		}

		char czyjRuch = w->ruch;

		//wykonujemy ruch
		w->wykonajRuch(ruch);
		w->przelaczUzytkownika();

		//glowna czesc algorytmu
		if (uzytkownik != czyjRuch) //gra przeciwnik
		{
			int ocena = MinMax(w, uzytkownik, poziom - 1, alfa, beta, wybranyRuch);

			if (beta >= ocena)
			{
				beta = ocena;
			}

			if (alfa >= beta)
			{
				stop = 1;
			}
		}
		else // my gramy
		{
			int ocena = MinMax(w, uzytkownik, poziom - 1, alfa, beta, wybranyRuch);

			if (alfa <= ocena)
			{
				alfa = ocena;
				(*wybranyRuch) = ruch;
			}
			if (alfa >= beta)
			{
				stop = 1;
			}
		}

		//cofamy ruch
		w->pola[ruch.poczatkowa.y][ruch.poczatkowa.x] = poczatek;
		w->pola[ruch.koncowa.y][ruch.koncowa.x] = koniec;
		if (ruch.zbity.x != -1 && ruch.zbity.y != -1)
		{
			w->pola[ruch.zbity.y][ruch.zbity.x] = zbity;
		}

		w->ruch = czyjRuch;
	}
	//zniszczliste(ruchy);
	if (uzytkownik != w->ruch) //gra przeciwnik
	{
		return beta;
	}
	else
	{
		return alfa;
	}
}



void ruchCzlowieka(Plansza* w)
{
	struct elementlisty* elem = NULL;
	MozliweRuchy ruchy(w);

	while (elem == NULL)
	{
		cout << "podaj wspolrzedne pionka:"<<endl;
		int x;
		int y;
		cin >> x >> y;
		struct pozycja poczatkowa = poz(x - 1, y - 1);
		cout << "podaj wspolrzedne docelowe pionka:" << endl;
		cin >> x >> y;
		struct pozycja koncowa = poz(x - 1, y - 1);

		elem = ruchy.dajMozliwyRuch(poczatkowa, koncowa);
		if (elem == NULL)
		{
			printf("nie mozna wykonac ruchu:\n");
		}
		else
		{
			w->wykonajRuch(elem->ruch);
		}
	}
}

void ruchKomputera(Plansza* w)
{
	cout << "ruch komputera:" << endl;
	struct ruch ruch;
	MinMax(w, w->ruch, 9, -1000, 1000, &ruch);
	w->wykonajRuch(ruch);
}

bool tura(struct Plansza* w, char uzytkownik)
{
	if (w->ruch == uzytkownik)
	{
		ruchCzlowieka(w);
	}
	else
	{
		ruchKomputera(w);

	}
	w->stan();

	bool gra = true;
	w->przelaczUzytkownika();
	MozliweRuchy ruchy(w);
	if (ruchy.brak())
	{
		if (w->ruch == uzytkownik)
		{
			cout << "Przegrales" << endl;

		}
		else
		{
			cout << "Wygrales" << endl;
		}
		gra = false;
	}
	
	return gra;
}


void main()
{
	srand(time(NULL));
	char uzytkownik;
	printf("wybierz kolor\n");
	printf("B - biale\n");
	printf("C - czarne\n");
	cin >> uzytkownik;

	Plansza warcaby(8,8);
	warcaby.stan();

	while (tura(&warcaby, uzytkownik));

	

}


