#pragma once
#include "RuchIPozycja.h"


class Plansza
{
private:
	int wiersze; // ilosc wierszy
	int kolumny; // ilosc kolumn

public:
	char** pola; // do utworzenia dwuwymiarowej tablicy dynamicznej
	char ruch; // zmienna informujaca czyj jest ruch

	Plansza(int wiersze, int kolumny); // kontsruktor 

	int ocena(char gracz); // zwraca stosunek naszych pionkow do pionkow prpzeciwnika
	
	void wykonajRuch(struct ruch ruch); // 
	
	bool naPoluJestPrzeciwnik(int x, int y); // sprawdza czy na danym polu jesy przeciwnik


	bool pustePole(int x, int y); // sprawdzamy, czy dane pole jest puste 

	bool naPlanszy(int x, int y); // zabezpieczenie przed wyjsciem poza plansze


	bool czyDamka(int x, int y); //czy nasza damka

	void przelaczUzytkownika(); // przelacza na ruch przeciwnego gracza

	void stan(); //stan ca�ej gry, rysuje plansze
		
	// zwraca ilosc wierszy
	int dajWiersze();

	// zwraca ilosc kolumn
	int dajKolumny();

private:
	void zamienNaDamke(int x, int y); // sprawdza ruch i w razie spelnienia warunku zamienia pion na damke
	void ustawieniePoczatkowe(); // ustawia wszytskie piony na poczatku gry we wlasciwe indeksy planszy
};