#include "Plansza.h"
#include <iostream>

using namespace std;

Plansza::Plansza(int wiersze, int kolumny)
{
	this->wiersze = wiersze;
	this->kolumny = kolumny;
	pola = new char* [wiersze];
	for (int i = 0; i < wiersze; i++)
	{
		pola[i] = new char[kolumny];
	}
	ustawieniePoczatkowe();
}

int Plansza::ocena(char gracz) // oceniamy przewage gracza
{
	char damka;
	char pionek;
	char damkaPrzeciwnika;
	char pionekPrzeciwnika;
	if (gracz == 'B')
	{
		damka = 'B';
		pionek = 'b';
		damkaPrzeciwnika = 'C';
		pionekPrzeciwnika = 'c';
	}
	else
	{
		damka = 'C';
		pionek = 'c';
		damkaPrzeciwnika = 'B';
		pionekPrzeciwnika = 'b';
	}
	int punkty = 0;
	for (int i = 0; i < wiersze; i++)
	{
		for (int j = 0; j < kolumny; j++)
		{
			if (pola[i][j] == damka)
			{
				punkty += 3;
			}
			else if (pola[i][j] == pionek)
			{
				punkty++;
			}
			if (pola[i][j] == damkaPrzeciwnika)
			{
				punkty -= 3;
			}
			else if (pola[i][j] == pionekPrzeciwnika)
			{
				punkty--;
			}

		}
	}
	return punkty;
}
void Plansza::zamienNaDamke(int x, int y)
{
	if (ruch == 'B' && y == wiersze - 1)
	{
		pola[y][x] = 'B';
	}
	if (ruch == 'C' && y == 0)
	{
		pola[y][x] = 'C';
	}
}

void Plansza::wykonajRuch(struct ruch ruch)
{
	pola[ruch.koncowa.y][ruch.koncowa.x] = pola[ruch.poczatkowa.y][ruch.poczatkowa.x]; //
	pola[ruch.poczatkowa.y][ruch.poczatkowa.x] = ' ';
	if (ruch.zbity.x != -1 && ruch.zbity.y != -1)
	{
		pola[ruch.zbity.y][ruch.zbity.x] = ' ';
	}
	zamienNaDamke(ruch.koncowa.x, ruch.koncowa.y);
}

bool Plansza::naPoluJestPrzeciwnik(int x, int y) // sprawdza czy na danym polu jesy przeciwnik
{
	if (ruch == 'B')
	{
		return (pola[y][x] == 'C' || pola[y][x] == 'c');
	}
	else
	{
		return (pola[y][x] == 'B' || pola[y][x] == 'b');
	}
}

bool Plansza::naPlanszy(int x, int y) // zabezpieczenie przed wyjsciem poza plansze
{
	if (x < 0 || x >= kolumny)
	{
		return false;
	}
	if (y < 0 || y >= wiersze)
	{
		return false;
	}
	return true;
}

bool Plansza::czyDamka(int x, int y)//czy nasza damka
{
	return pola[y][x] == ruch;
}

void Plansza::przelaczUzytkownika() // przelacza na ruch przeciwnego gracza
{
	if (ruch == 'B')
	{
		ruch = 'C';
	}
	else
	{
		ruch = 'B';
	}
}

void Plansza::stan()
{
	cout << "  ";
	
	for (int k = 0; k < kolumny; k++)
	{
		cout << " " << k + 1;
	}
	cout << endl;
	
	for (int y = 0; y < wiersze; y++)
	{

		for (int k = kolumny + 1; k > 0; k--)
		{
			cout << "+-";
		}
		cout << "+" << endl;
		cout << y + 1 << " ";
		for (int x = 0; x < kolumny; x++)
		{
			cout << "|" << pola[y][x];
		}
		cout << "|" << endl;
	}
	for (int k = kolumny + 1; k > 0; k--)
	{
		cout << "+-";
	}
	cout << "+" << endl;
}

	bool Plansza::pustePole(int x, int y) // sprawdzamy, czy dane pole jest puste 
	{
		return pola[y][x] == ' ';
	}

void Plansza::ustawieniePoczatkowe()
{
	for (int i = 0; i < wiersze; i++) {
		for (int j = 0; j < kolumny; j++)
			pola[i][j] = ' ';
	}
	//bia�e 1.rz�d
	for (int i = 0; i < kolumny; i = i + 2)
		pola[0][i] = 'b';
	//bia�e 2.rz�d
	for (int i = 1; i < kolumny; i = i + 2)
		pola[1][i] = 'b';
	//bia�e 3.rz�d
	for (int i = 0; i < kolumny; i = i + 2)
		pola[2][i] = 'b';
	//czarne 1.rz�d
	for (int i = 1; i < kolumny; i = i + 2)
		pola[wiersze - 1][i] = 'c';
	//czarne 2.rz�d
	for (int i = 0; i < kolumny; i = i + 2)
		pola[wiersze - 2][i] = 'c';
	//czarne 3.rz�d
	for (int i = 1; i < kolumny; i = i + 2)
		pola[wiersze - 3][i] = 'c';
	ruch = 'B';
}

int Plansza::dajWiersze()
{
	return wiersze;
}

int Plansza::dajKolumny()
{
	return kolumny;
}