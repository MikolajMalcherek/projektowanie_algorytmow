#pragma once
#include "RuchIPozycja.h"
#include "Plansza.h"
#include <stdlib.h> 

struct elementlisty
{
	struct ruch ruch;
	struct elementlisty* nast;
};

class MozliweRuchy
{
private:
	struct elementlisty* ruchy;
	Plansza* w;
public:

	MozliweRuchy(Plansza* w);

	~MozliweRuchy();

	struct elementlisty* dajPoczatek();

	bool brak();

	elementlisty* dajMozliwyRuch(struct pozycja poczatkowa, struct pozycja koncowa);


	void zniszczliste(struct elementlisty* ptr);


	elementlisty* mozliweruchyPionkiem(Plansza* w, struct elementlisty* ruchy, int x, int y);


	elementlisty* ruchDamka(Plansza* w, struct elementlisty* ruchy, int x, int y, int kierunekX, int kierunekY);


	elementlisty* dodajRuch(Plansza* w, struct elementlisty* ruchy, struct pozycja poczatkowa, struct pozycja koncowa);

	
	int Y(int y, int przesuniecie);

	pozycja poz(int x, int y);
	
};

