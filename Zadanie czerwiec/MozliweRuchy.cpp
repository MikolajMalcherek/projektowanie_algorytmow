#include "MozliweRuchy.h"

MozliweRuchy::MozliweRuchy(Plansza* w)
	{
	this->w = w;
	ruchy = NULL;
	for (int y = 0; y < w->dajWiersze(); y++)
	{
		for (int x = 0; x < w->dajKolumny(); x++)
		{
			//jezeli na polu jest pionek aktualnego gracza
			if (!w->pustePole(x, y) && !w->naPoluJestPrzeciwnik(x, y))
			{
				ruchy = mozliweruchyPionkiem(w, ruchy, x, y);
			}
		}
	}
	}

MozliweRuchy::~MozliweRuchy()
{
	zniszczliste(ruchy);
}

elementlisty* MozliweRuchy::dajPoczatek()
{
	return ruchy;
}

bool MozliweRuchy::brak()
{
	return ruchy == NULL;
}

elementlisty* MozliweRuchy::dajMozliwyRuch(struct pozycja poczatkowa, struct pozycja koncowa)
{
	struct elementlisty* pom = ruchy;
	while (pom != NULL)
	{
		if (pom->ruch.poczatkowa.x == poczatkowa.x && pom->ruch.poczatkowa.y == poczatkowa.y
			&& pom->ruch.koncowa.x == koncowa.x && pom->ruch.koncowa.y == koncowa.y)
		{
			return pom;
		}
		pom = pom->nast;
	}
	return NULL;
}


elementlisty* MozliweRuchy::mozliweruchyPionkiem(Plansza* w, struct elementlisty* ruchy, int x, int y)
{
	if (!w->czyDamka(x, y)) // jesli nie ma damki na tym polu
	{
		if (w->naPlanszy(x - 1, Y(y, 1)) && w->pustePole(x - 1, Y(y, 1))) // czy w lewo do gory nie jest poza plansza i czy jest puste pole
		{
			ruchy = dodajRuch(w, ruchy, poz(x, y), poz(x - 1, Y(y, 1)));
		}

		if (w->naPlanszy(x + 1, Y(y, 1)) && w->pustePole(x + 1, Y(y, 1))) // czy w prawo do gory nie jest poza plansza i czy jest puste pole
		{
			ruchy = dodajRuch(w, ruchy, poz(x, y), poz(x + 1, Y(y, 1)));
		}

		if (w->naPlanszy(x - 1, Y(y, 1)) && w->naPlanszy(x - 2, Y(y, 2)) // czy w lewo do gory i w lewo dwa do gory nie jest poza plansza i czy jest puste pole w lewo dwa do gory
			&& w->naPoluJestPrzeciwnik(x - 1, Y(y, 1)) && w->pustePole(x - 2, Y(y, 2)))
		{
			ruchy = dodajRuch(w, ruchy, poz(x, y), poz(x - 2, Y(y, 2)));
			ruchy->ruch.zbity.x = x - 1;
			ruchy->ruch.zbity.y = Y(y, 1);
		}

		if (w->naPlanszy(x + 1, Y(y, 1)) && w->naPlanszy(x + 2, Y(y, 2))
			&& w->naPoluJestPrzeciwnik(x + 1, Y(y, 1)) && w->pustePole(x + 2, Y(y, 2)))
		{
			ruchy = dodajRuch(w, ruchy, poz(x, y), poz(x + 2, Y(y, 2)));
			ruchy->ruch.zbity.x = x + 1;
			ruchy->ruch.zbity.y = Y(y, 1);
		}
	}
	else // jesli jest to damka
	{
		ruchy = ruchDamka(w, ruchy, x, y, -1, -1);
		ruchy = ruchDamka(w, ruchy, x, y, 1, -1);
		ruchy = ruchDamka(w, ruchy, x, y, -1, 1);
		ruchy = ruchDamka(w, ruchy, x, y, 1, 1);
	}
	return ruchy;
}


elementlisty* MozliweRuchy::ruchDamka(Plansza* w, struct elementlisty* ruchy, int x, int y, int kierunekX, int kierunekY)
{
	struct pozycja zbity;
	zbity.x = -1;
	zbity.y = -1;
	for (int i = 1; true; i++)
	{
		if (w->naPlanszy(x + kierunekX * i, y + kierunekY * i)) // jesli ruch nie wykracza poza plansze
		{
			if (w->pustePole(x + kierunekX * i, y + kierunekY * i)) // jesli pole jest puste
			{
				ruchy = dodajRuch(w, ruchy, poz(x, y), poz(x + kierunekX * i, y + kierunekY * i));
			}
			else if (w->naPoluJestPrzeciwnik(x + kierunekX * i, y + kierunekY * i)) // jesli na polu stoi przeciwnik
			{
				zbity.x = x + kierunekX * i;
				zbity.y = y + kierunekY * i;
				break;
			}
			else // na polu jest nasz pionek
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
	if (zbity.x != -1 && zbity.y != -1) // natrafilismy na pionek przeciwnika
	{
		for (int i = 1; 1; i++)
		{
			if (w->naPlanszy(zbity.x + kierunekX * i, zbity.y + kierunekY * i))
			{
				if (w->pustePole(zbity.x + kierunekX * i, zbity.y + kierunekY * i)) // jesli kolejne pola sa puste, dodajemy je do mozliwych, jesli stoi przeciwnik przerywamy dzialanie
				{
					ruchy = dodajRuch(w, ruchy, poz(x, y), poz(zbity.x + kierunekX * i, zbity.y + kierunekY * i));
					ruchy->ruch.zbity = zbity;
				}
				else
				{
					break;
				}
			}
			else
			{
				break;
			}
		}
	}
	return ruchy;
}

// dodaje mozliwy ruch do listy
elementlisty* MozliweRuchy::dodajRuch(Plansza* w, elementlisty* ruchy, pozycja poczatkowa, pozycja koncowa)
	{
		struct elementlisty* nowy = new struct elementlisty;
		nowy->ruch.poczatkowa = poczatkowa;
		nowy->ruch.koncowa = koncowa;
		nowy->ruch.zbity.x = -1;
		nowy->ruch.zbity.y = -1;
		nowy->nast = ruchy;
		ruchy = nowy;
		return ruchy;
	}


int MozliweRuchy::Y(int y, int przesuniecie)
{
	if (w->ruch == 'B')
	{
		return y + przesuniecie;
	}
	else
	{
		return y - przesuniecie;
	}
}


pozycja MozliweRuchy::poz(int x, int y)
{
	struct pozycja pozycja;
	pozycja.x = x;
	pozycja.y = y;
	return pozycja;
}

void MozliweRuchy::zniszczliste(struct elementlisty* ptr)
{
	if (ptr)
	{
		zniszczliste(ptr->nast);
		free(ptr);
	}
}