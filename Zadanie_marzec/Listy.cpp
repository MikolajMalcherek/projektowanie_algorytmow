﻿#include <iostream>
using namespace std;

class wezel
{
private:
    wezel* nastepny_wezel;
    int liczba;
public:


    // zwraca wskaznik na kolejny(starszy) wezel
    wezel* get_next();

    // zwraca wartosc liczby w wezle
    int get_liczba();

    // wstawia liczbe do wezla
    void set_liczba(int wartosc);

    // ustawia wskaznik na kolejny wezel
    void set_next(wezel* wsk_na_nastepny_wezel);
};

/*                               Metody klasy wezel                              */
wezel* wezel::get_next()
{
    return nastepny_wezel;
}

int wezel::get_liczba()
{
    return liczba;
}

void wezel::set_liczba(int wartosc)
{
    liczba = wartosc;
}


//
void wezel::set_next(wezel* wsk_na_nastepny_wezel)
{
    nastepny_wezel = wsk_na_nastepny_wezel;
}
// przed usunieciem 
//wezel usuwamy delete
// wezel *nowy_element = new wezel;


class lista
{
private:
    wezel* head;

public:
    void inicjalizuj();

    // dodaje wezel na liste
    void add(int liczba);

    // wyswietla caly stos
    void wyswietl();

    // usuwa element ze szczytu stosu
    bool pop(int& wartosc);

    // usuwa wszystkie elementy stosu
    void clear();
};

void lista::inicjalizuj()
{
    head = NULL;
}

void lista::add(int liczba)
{
    wezel* nowy = new wezel;   // alokujemy pamiec dla nowego wezla
    nowy->set_liczba(liczba);  // w nowym wezle w miejscu gdzie ma byc liczba wpisujemy liczbe
    nowy->set_next(head);      // wskaznik nastepny_wezel klasy wezel nowego wezla wskazuje na to, na co wskazuje head
    head = nowy;               // head wskazuje na nowy wezel
}

bool lista::pop(int& wartosc)
{
    // tworzymy wskaznik na zmienna typu wezel
    if (head == NULL)
    {
        cout << "Stos jest pusty!" << endl;
        return false;
    }

    // wskaznik na zmienna typu wezel przyrownujemy do head, zeby head moc przyrownac do nastepnego wskazywanego
    // przez niego wezla
    wezel* usuwanie = head;
    head = usuwanie->get_next();
    wartosc = usuwanie->get_liczba();
    
    delete usuwanie;
    return true;

}

void lista::clear()
{
    wezel* usuwanie;
    while (head != NULL)
    {
        usuwanie = head;
        head = usuwanie->get_next();
        delete usuwanie;
    }
}

void lista::wyswietl()
{
    wezel* wskaznik;
    wskaznik = head;

    while (wskaznik != NULL)
    {
        cout << "|" << wskaznik->get_liczba() << "|" << endl;

        // getnext() zwraca wskaznik na poprzedni wezel, wiec przyrownujac go wskaznik zaczyna wskazywac na poprzedni wezel
        wskaznik = wskaznik->get_next();
    }
}





int main()
{
    lista stos;
    int liczba;
    stos.inicjalizuj();
    int wybor;

    while (1)
    {
        cout << "1 - Dodaj element: " << endl;
        cout << "2 - Usun element: " << endl;
        cout << "3 - wyswietl stos: " << endl;
        cout << "4 - usun wszystkie elementy ze stosu: " << endl;
        cout << "5 - zakoncz program: " << endl;
        cout << "Podaj opcję: ";
        cin >> wybor;
        switch (wybor)
        {
        case 1:
            cout << "Podaj liczbe: ";
            cin >> liczba;
            cout << endl;
            stos.add(liczba);
            break;

        case 2:
            
            stos.pop(liczba);
            cout << "pobrana liczba " <<liczba << endl;
            break;

        case 3:
            stos.wyswietl();
            break;

        case 4:
            stos.clear();
            break;

        case 5:
            stos.clear();
            return 0;
            break;
        defeault: cout << "Nie ma takiej opreacji." << endl;
        }
        
    }
    


}