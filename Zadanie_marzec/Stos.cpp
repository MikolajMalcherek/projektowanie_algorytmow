﻿#include <iostream>
//biblioteki do pomiaru czasu działania programu
#include<cstdlib>
#include<time.h>

using namespace std;

struct Stos
{
    int* tablica;
    int maxIlosc; // zmienna do okreslania rozmiaru stosu, po którym wykonamy realokację
    int ilosc; // ilosc elementow na stosie
    int granicaZmianyWielkosci;  // zmienna służąca do kontrolowania ponownego realokowania pamieci
                                // im większa ta zmienna, tym rzadziej będziemy alokować na nowo tablice
                                // ale tez bedziemy mieli niewykorzystana pamiec, co przy dodawaniu wiekszej ilosci 
                                // elementow jest znacznie szybsze
};

/* DODAWANIE ELEMENTU NA SZCZYT STOSU */

void push(Stos& stos,int wartosc ) // liczba wpisana na stos, aktualny rozmiar stosu, tablica z liczbami 
{
    // jesli rozmiar stosu bedzie rowny wartosci maxIlosc to dopiero realokujemy pamiec, jest to szybsze 
    // przy dodawaniu wiekszej ilosci elementow
    if (stos.ilosc == stos.maxIlosc)
    {
        int nowyRozmiar = stos.maxIlosc + stos.granicaZmianyWielkosci;
        int* zamiennik = new int[nowyRozmiar];
        for (int i = 0; i < stos.ilosc; i++)
        {
            zamiennik[i] = stos.tablica[i];
        }
        delete[] stos.tablica;
        stos.tablica = zamiennik;
        stos.maxIlosc = nowyRozmiar;
    }
    stos.tablica[stos.ilosc] = wartosc;
    stos.ilosc++;
}


//Funkcja pop - wpisujemy do nowej tablicy dynamicznej o rozmiarze o 1 mniejszym niz oryginal wartosci oryginalu 
// oprocz elementu w zerowym indeksie, a nastepnie usuwamy oryginal i przypisujemy go do zamiennika

bool pop(Stos& stos, int& wartosc)
{
    // zabezpieczenie przed usuwaniem elementu z pustego stosu
    if (stos.ilosc == 0)
    {
        cout << "Nie można usunac elementu - stos jest pusty." << endl;
        return false;
    }

    // zapisujemy wartosc usunietego elementu
    wartosc = stos.tablica[stos.ilosc - 1];
    stos.ilosc--;

    // jesli jest wiecej niewykorzystanych zmiennych niz wartosc granicaZmianyWartosci
    // to alokujemy tablice o rozmiarze mniejszym o wartosc tej zmiennej
    if (stos.maxIlosc - stos.ilosc > stos.granicaZmianyWielkosci)
    {
        int nowyRozmiar = stos.maxIlosc - stos.granicaZmianyWielkosci;
        int* zamiennik = new int[nowyRozmiar];
        for (int i = 0; i < stos.ilosc; i++)
        {
            zamiennik[i] = stos.tablica[i];
        }
        delete[] stos.tablica;
        stos.tablica = zamiennik;
        // zmieniamy wartość maksymalnej ilosci elementow na stosie, dla której ponownie zrealokujemy tablicę
        stos.maxIlosc = nowyRozmiar;
    }
    
    return true;

}



void wyswietl(Stos stos)
{
    if (stos.ilosc == 0)
    {
        cout << "Stos jest pusty." << endl;
        return;
    }
    cout << "Zawartosc stosu: " << endl;


    // wyswietla i i nformuje, że ten element jest na gorze stosu
    cout << "|" << stos.tablica[stos.ilosc-1] << "|" << " <--- gorny elementy stosu" << endl;
    for (int i = stos.ilosc-2; i >=0; i--)
    {
        cout << "|" << stos.tablica[i] << "|" << endl;
    }
    return;
}


void test1()
{
    Stos stos;
    stos.ilosc = 0;
    stos.maxIlosc = 10;
    stos.tablica = new int[10];
    stos.granicaZmianyWielkosci = 5;
    
    clock_t start, stop;
    int x;
    start = clock();
    for (int i = 0; i < 100; i++)
    {
        push(stos, i);
    }
    stop = clock();
    double czas = (double)(stop - start) / CLOCKS_PER_SEC;
    cout << "czas działania programu: " << czas << endl;
    delete stos.tablica;
}
void test2()
{
    Stos stos;
    stos.ilosc = 0;
    stos.maxIlosc = 10;
    stos.tablica = new int[10];
    stos.granicaZmianyWielkosci = 1000;

    clock_t start, stop;
    int x;
    start = clock();
    for (int i = 0; i < 100000; i++)
    {
        push(stos, i);
    }
    for (int i = 0; i < 100000; i++)
    {
        pop(stos, x);
    }
    stop = clock();
    double czas = (double)(stop - start) / CLOCKS_PER_SEC;
    cout << "czas działania programu: " << czas << endl;
    delete stos.tablica;
}


int main()
{
    Stos stos;
    stos.ilosc = 0;
    stos.maxIlosc = 10;
    stos.tablica = new int[10];
    stos.granicaZmianyWielkosci = 5;
    int wartosc;

    while (1)
    {
        test1();
        cout << "Rozmiar stosu: " << stos.ilosc << endl;
        cout << "Prosze wybrac opreacje: " << endl;
        cout << "1 - dodawanie elementu na stos - 1" << endl;
        cout << "2 - wyswietlanie stosu - 2" << endl;
        cout << "3 - usuwanie elementu ze sotsu - 3" << endl;
        cout << "4 - zakoncz program - 4" << endl;
        int wybor;
        cin >> wybor;
        switch (wybor)
        {
        case 1:
            cout << "Podaj liczbę: ";
            cin >> wartosc;
            push(stos, wartosc);
            break;
        case 2:
            wyswietl(stos);
            break;
        case 3:
            if (pop(stos, wartosc))
            {
                cout << "pobrana wartosc:" << wartosc << endl;
            }
            
            break;
        case 4:
            return 1;
            break;

        defeault: cout << "Nie ma takiej opcji";
        }


    }
    
    delete stos.tablica;
}



